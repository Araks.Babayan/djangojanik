from django.db import models



class Computer(models.Model):
    name = models.CharField(blank=True, null=True, max_length=50)
    power = models.CharField(blank=True, null=True,max_length=50)
    color = models.CharField(blank=True, null=True, max_length=50)
    price = models.IntegerField(blank=True, null=True)

    def __str__(self):
        return self.name




class Stationery(models.Model):
    name = models.CharField(blank=True, null=True, max_length=50)
    price = models.IntegerField(blank=True, null=True)

    def __str__(self):
        return self.name
