from rest_framework import serializers
from .models import Computer, Stationery



class ComputerSerialaizer(serializers.ModelSerializer):
    class Meta:
        model = Computer
        fields = '__all__'

        verbose_name = 'Computer'
        verbose_name_plural = 'Computers'


class StationarySerialaizer(serializers.ModelSerializer):
    class Meta:
        model = Stationery
        fields = '__all__'

        verbose_name = 'Stationery'
        verbose_name_plural = 'Stationery'

