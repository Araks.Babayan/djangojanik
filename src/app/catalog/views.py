from django.shortcuts import render
from .serializers  import ComputerSerialaizer, StationerySerializers
from rest_framework import viewsets
from .models import Computer, Stationery
#from .permissions import IsMandarin
from rest_framework.permissions import DjangoModelPermissions


# Create your views here.
class ComputerViewSet(viewsets.ModelViewSet):
    queryset = Computer.objects.all()
    serializer_class = ComputerSerialaizer
    permission_classes = [DjangoModelPermissions]

class StationeryViewSet(viewsets.ModelViewSet):
    queryset = Stationery.objects.all()
    serializer_class = StationerySerialaizers
    permission_classes = [DjangoModelPermissions]
    
