from django.contrib import admin
from .models import Computer, Stationery


@admin.register(Computer)
class Computer(admin.ModelAdmin):
    list_display = ('name', 'price')

@admin.register(Stationery)
class Station(admin.ModelAdmin):
    list_display = ('name', 'price') 